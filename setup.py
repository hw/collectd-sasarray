from setuptools import setup, find_packages
import sys
import os

setup(
    name='collectd_sasarray',
    version='22.1.3',
    description="Collectd Plugin to monitor SAS Expander and driver arrays",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: System :: Monitoring",
    ],
    keywords='collectd sas drives monitoring',
    url='https://gitlab.cern.ch/hw/collectd-sasarray',
    author='Luca Gardi',
    author_email='luca.gardi@cern.ch',
    maintainer='CERN IT Procurement',
    maintainer_email='procurement-team@cern.ch',
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
)
