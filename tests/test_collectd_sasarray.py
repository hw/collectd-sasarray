""" Unit testing for collectd_sasarray. """
import sys
import pytest
from mock import MagicMock, Mock, patch, call

import json
@pytest.fixture
def csas():
    """ Mock collectd module. """
    collectd = MagicMock()

    # Mock open
    class MockOpen():
        """ This Context Manager class mocks builtin open. """
        ESC_FAILURE_MESSAGES_CONTENT_FILE = 'esc_failure_messages_json/default'
        EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/default'
        PCI_IDS_EXCLUSION_LIST_CONTENT_FILE = 'pci_ids_exclusion_list_json/default'
        HARDWARE_FACTS_CACHE_CONTENT_FILE = 'hardware_facts_cache_yaml/default'
        def read(self, *args, **kargs):
            return self.file.read()

        def __exit__(self, *args):
            self.file.close()

        def __enter__(self):
            return self.file

        def __init__(self, *args, **kargs):
            if args[0] == '/etc/collectd-sasarray/esc_failure_messages.json':
                self.file = open(self.ESC_FAILURE_MESSAGES_CONTENT_FILE)
            elif args[0] == '/etc/collectd-sasarray/exceptions_list.json':
                self.file = open(self.EXCEPTIONS_LIST_CONTENT_FILE)
            elif args[0] == '/etc/collectd-sasarray/pci_ids_exclusion_list.json':
                self.file = open(self.PCI_IDS_EXCLUSION_LIST_CONTENT_FILE)
            elif args[0] == '/opt/puppetlabs/puppet/cache/hardware_facts_cache.yaml':
                self.file = open(self.HARDWARE_FACTS_CACHE_CONTENT_FILE)
            else:
                print('MockOpen called: %s %s' % (args, kargs))

    # Mock subprocess
    class MockPopen():
        """ This class mocks subprocess.Popen """
        SERVER_ID_STDOUT_READLINE_CONTENT = 'UNITTEST-TESTNODE1-1\n'
        LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/default'
        SAS2IRCU_LIST_STDOUT_READ_CONTENT_FILE = 'sas2ircu/list_default'
        SAS2IRCU_LIST_STDERR_READ_CONTENT_FILE = 'empty'
        SAS2IRCU_0_DISPLAY_STDOUT_READ_CONTENT_FILE = 'sas2ircu/0_display_default'
        SAS2IRCU_0_DISPLAY_STDERR_READ_CONTENT_FILE = 'empty'
        DEFAULT_STDERR_CONTENT = ''

        def MockStdoutRead(self, *args, **kargs):
            return self.stdout_read_return

        def MockStdoutReadline(self, *args, **kargs):
            return self.stdout_readline_return
        
        def MockStdoutReadlines(self, *args, **kargs):
            return self.stdout_readlines_return 
        
        def MockStderrRead(self, *args, **kargs):
            return self.stderr_read_return

        def MockStderrReadline(self, *args, **kargs):
            return self.stderr_readline_return
        
        def MockStderrReadlines(self, *args, **kargs):
            return self.stderr_readlines_return

        def __init__(self, *args, **kargs):
            # Setting default stderr return
            self.stderr_readlines_return = self.DEFAULT_STDERR_CONTENT
            self.stderr_readline_return = self.DEFAULT_STDERR_CONTENT
            self.stderr_read_return = self.DEFAULT_STDERR_CONTENT

            # Mocking calls
            if args[0] == ['/opt/puppetlabs/bin/facter', '-p', 'productserialassettag']:
                self.stdout_readline_return = self.SERVER_ID_STDOUT_READLINE_CONTENT
                if self.SERVER_ID_STDOUT_READLINE_CONTENT == 'exception':
                    raise IOError('Test Exception')
            elif args[0] == '/usr/sbin/lsmod':
                self.stdout_read_return = open(self.LSMOD_STDOUT_READ_CONTENT_FILE, 'r').read()
            elif args[0] == ['/usr/sbin/sas2ircu', 'list']:
                self.stdout_read_return = open(self.SAS2IRCU_LIST_STDOUT_READ_CONTENT_FILE, 'r').read()
                self.stderr_read_return = open(self.SAS2IRCU_LIST_STDERR_READ_CONTENT_FILE, 'r').read()
            elif args[0] == ['/usr/sbin/sas2ircu', '0', 'display']:
                self.stdout_read_return = open(self.SAS2IRCU_0_DISPLAY_STDOUT_READ_CONTENT_FILE, 'r').read()
                self.stderr_read_return = open(self.SAS2IRCU_0_DISPLAY_STDERR_READ_CONTENT_FILE, 'r').read()
            else:
                print('MockPopen called: %s %s' % (args, kargs))

            # Mock stdout
            self.stdout = MagicMock()
            self.stdout.read = self.MockStdoutRead
            self.stdout.readline = self.MockStdoutReadline
            self.stdout.readlines = self.MockStdoutReadlines
            # Mock stderr
            self.stderr = MagicMock()
            self.stderr.read = self.MockStderrRead
            self.stderr.readline = self.MockStderrReadline

    mock_subprocess = MagicMock()
    mock_subprocess.Popen = MockPopen

    # Mock SsCli
    class MockSsCli():
        """ This class mocks SsCli provided by the sscli.s2cli package. """
        S2CLI_READ_CONTENT_FILE = 's2cli/default.json'
         
        def __init__(self, excluded_pci_controllers):
            self.excluded_pci_controllers = excluded_pci_controllers
            self.attached_storage_return_file = self.S2CLI_READ_CONTENT_FILE

        def get_attached_storage_subsystem(self):
            if self.S2CLI_READ_CONTENT_FILE == None:
                return None
            return json.loads(open(self.attached_storage_return_file).read())

    mock_s2cli = MagicMock()
    mock_s2cli.SsCli = MockSsCli

    with patch.dict('sys.modules', {'collectd': collectd,
                                    'subprocess': mock_subprocess,
                                    'sscli': MagicMock(),
                                    'sscli.s2cli': mock_s2cli}):
        
        # Import sensor before patching
        import collectd_sasarray

        with patch('collectd_sasarray.open', MockOpen, create=True):             
            yield collectd_sasarray


def test_plugin_registration(csas):
    """ Test if the plugin is registered successfully """
    csas.collectd.register_config_assert_called_once_with(
        csas.configure_callback)


def test_configure_defaults(csas):
    """ Test if the plugin runs fine without parameters. """
    csas.configure_callback(Mock(children=[]))
    assert csas.INTERVAL == 300
    csas.collectd.register_read.assert_called_once_with(csas.read_callback, 300)


@pytest.mark.parametrize("interval", [(100)])
def test_configure_parameters(csas, interval):
    """ Test if configure callback works properly and parameters are set. """
    csas.configure_callback(
        Mock(children=[Mock(key='INTERVAL', values=[interval])]))
    assert csas.INTERVAL == interval
    csas.collectd.register_read.assert_called_once_with(csas.read_callback, interval)

def test_server_id_from_puppet_cache(csas):
    assert 'UNITTEST-TESTNODE1-1' == (csas.get_server_id_from_puppet_cache())

def test_server_id_from_puppet_cache_empty(csas):
    csas.open.HARDWARE_FACTS_CACHE_CONTENT_FILE = 'empty'
    assert not csas.get_server_id_from_puppet_cache()
    csas.collectd.error.assert_has_calls([call("empty file or can't find key productserialassettag in /opt/puppetlabs/puppet/cache/hardware_facts_cache.yaml")])

def test_server_id_from_puppet_cache_ioerror(csas):
    csas.open.HARDWARE_FACTS_CACHE_CONTENT_FILE = 'lol'
    assert not csas.get_server_id_from_puppet_cache()
    csas.collectd.error.assert_has_calls([call("can't find file /opt/puppetlabs/puppet/cache/hardware_facts_cache.yaml")])

def test_facter_exception(csas):
    # fail puppet cache retrieve
    csas.open.HARDWARE_FACTS_CACHE_CONTENT_FILE = 'empty'

    csas.subprocess.Popen.SERVER_ID_STDOUT_READLINE_CONTENT='exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call('exception while retrieving facter output: Test Exception'),
        call("can't get key 'productserialassettag' from facter or Puppet cache"),
        call("can't parse serial and asset tag from False")])

def test_facter_undef(csas):
    # fail puppet cache retrieve
    csas.open.HARDWARE_FACTS_CACHE_CONTENT_FILE = 'empty'

    csas.subprocess.Popen.SERVER_ID_STDOUT_READLINE_CONTENT = 'UNDEF-UNDEF'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("empty file or can't find key productserialassettag in /opt/puppetlabs/puppet/cache/hardware_facts_cache.yaml"),
        call("invalid output of '/opt/puppetlabs/bin/facter -p productserialassettag': UNDEF-UNDEF"),
        call("can't get key 'productserialassettag' from facter or Puppet cache"),
        call("can't parse serial and asset tag from False")])


def test_lsmod_exception(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("exception while retrieving lsmod output: [Errno 2] No such file or directory: 'lsmod/exception'"),
        call("can't get loaded modules with /usr/sbin/lsmod")])


def test_lsmod_empty(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'empty'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call('lsmod returned an invalid output'),
        call("can't get loaded modules with /usr/sbin/lsmod")])


def test_esc_failure_json_exception(csas):
    csas.open.ESC_FAILURE_MESSAGES_CONTENT_FILE = 'exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("exception while retrieving config files: [Errno 2] No such file or directory: 'exception'"),
        call("can't get ESC failure messages list from /etc/collectd-sasarray/esc_failure_messages.json")])


def test_exceptions_list_json_exception(csas):
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([call("can't get exception list from /etc/collectd-sasarray/exceptions_list.json")])


def test_pci_ids_exclusion_list_json_exception(csas):
    csas.open.PCI_IDS_EXCLUSION_LIST_CONTENT_FILE = 'exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("exception while retrieving config files: [Errno 2] No such file or directory: 'exception'"),
        call("can't get PCI IDs exclusion list from /etc/collectd-sasarray/pci_ids_exclusion_list.json")])


def test_esc_failure_json_empty(csas):
    csas.open.ESC_FAILURE_MESSAGES_CONTENT_FILE = 'empty'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call('exception while parsing a config files: JSON is not valid'),
        call("can't get ESC failure messages list from /etc/collectd-sasarray/esc_failure_messages.json")])

def test_exceptions_list_json_empty(csas):
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'empty'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([call("can't get exception list from /etc/collectd-sasarray/exceptions_list.json")])


def test_pci_ids_exclusion_list_json_empty(csas):
    csas.open.PCI_IDS_EXCLUSION_LIST_CONTENT_FILE = 'empty'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call('exception while parsing a config files: JSON is not valid'),
        call("can't get PCI IDs exclusion list from /etc/collectd-sasarray/pci_ids_exclusion_list.json")])


def test_loaded_hba_drivers_all(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.configure_callback(Mock(children=[]))

    assert csas.LOADED_HBA_DRIVERS == ['mpt3sas', 'mpt2sas', 'pm80xx']


def test_loaded_hba_drivers_none(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/no_drivers'
    csas.configure_callback(Mock(children=[]))

    assert csas.LOADED_HBA_DRIVERS == []
    csas.collectd.error.assert_has_calls([
        call("no supported HBA drivers loaded, skipping read callback")])


def test_sas2ircu_list_empty(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_LIST_STDOUT_READ_CONTENT_FILE = 'empty'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("sas2ircu list' stdout is empty"),
        call("can't detect LSI HBA RAID support (mpt2sas only)")])


def test_sas2ircu_list_stderr(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_LIST_STDERR_READ_CONTENT_FILE = 'sas2ircu/stderr'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("'sas2ircu list' stderr is not empty: I AM NOT EMPTY"),
        call("can't detect LSI HBA RAID support (mpt2sas only)")])


def test_sas2ircu_list_exception(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_LIST_STDOUT_READ_CONTENT_FILE = 'exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("exception while retrieveing '/usr/sbin/sas2ircu list': [Errno 2] No such file or directory: 'exception'"),
        call("can't detect LSI HBA RAID support (mpt2sas only)")])


def test_sas2ircu_display_empty(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_0_DISPLAY_STDOUT_READ_CONTENT_FILE = 'empty'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("sas2ircu 0 display' stdout is empty"),
        call("can't detect LSI HBA RAID support (mpt2sas only)")])


def test_sas2ircu_display_stderr(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_0_DISPLAY_STDERR_READ_CONTENT_FILE = 'sas2ircu/stderr'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("'sas2ircu 0 display' stderr is not empty: I AM NOT EMPTY"),
        call("can't detect LSI HBA RAID support (mpt2sas only)")])


def test_sas2ircu_display_exception(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_0_DISPLAY_STDOUT_READ_CONTENT_FILE = 'exception'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.error.assert_has_calls([
        call("exception while retrieveing '/usr/sbin/sas2ircu 0 display': [Errno 2] No such file or directory: 'exception'"),
        call("can't detect LSI HBA RAID support (mpt2sas only)")])

def test_raid_support_enabled_dell_perc(csas):
    csas.subprocess.Popen.LSMOD_STDOUT_READ_CONTENT_FILE = 'lsmod/all_drivers'
    csas.subprocess.Popen.SAS2IRCU_0_DISPLAY_STDOUT_READ_CONTENT_FILE = 'sas2ircu/0_display_raid_enabled'
    csas.configure_callback(Mock(children=[]))

    csas.collectd.info.assert_has_calls([call('excluding LSI HBA because RAID support is Enabled (DELL PERC node?)')])
    assert csas.LOADED_HBA_DRIVERS == ['mpt3sas', 'pm80xx']

def test_pci_ids_exclusion_list_json_no_exclusion(csas):
    csas.open.PCI_IDS_EXCLUSION_LIST_CONTENT_FILE = 'pci_ids_exclusion_list_json/default'
    csas.configure_callback(Mock(children=[]))

    assert csas.EXCLUDED_PCI_CONTROLLERS == []


def test_pci_ids_exclusion_list_json_server_id_match(csas):
    csas.open.PCI_IDS_EXCLUSION_LIST_CONTENT_FILE = 'pci_ids_exclusion_list_json/server_id_match'
    csas.configure_callback(Mock(children=[]))

    assert csas.EXCLUDED_PCI_CONTROLLERS == ['02.00.0']


def test_pci_ids_exclusion_list_json_contract_id_match(csas):
    csas.open.PCI_IDS_EXCLUSION_LIST_CONTENT_FILE = 'pci_ids_exclusion_list_json/contract_id_match'
    csas.configure_callback(Mock(children=[]))

    assert csas.EXCLUDED_PCI_CONTROLLERS == ['03.00.0']


def test_dispatch_enclosures(csas):
    # Providing two enclosures to the node, expecting at least 1
    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        calls = [
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0])]
        val_mock.assert_has_calls(calls, any_order=True)

def test_dispatch_enclosures_exception_list(csas):
    # Providing two enclosures to the node, expecting 0
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/0_enclosures'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        calls = [
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[1])]
        val_mock.assert_has_calls(calls, any_order=True)


def test_dispatch_no_scsi_host(csas):
    # Providing no SCSI hosts, expected 1 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_scsi_host.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[1])],
            any_order=True)

        csas.collectd.error.assert_has_calls(
            [call(u'PCI controller 02:00.0 has no SCSI host mapped to it'),
            call(u'PCI controller 02:00.0 has no SCSI host mapped to it'),
            call(u'PCI controller 02:00.0 has no SCSI host mapped to it')])


def test_dispatch_no_scsi_attached_storage(csas):
    # Providing no SCSI hosts, expected 1 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = None

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        csas.collectd.info.assert_has_calls([call(u'collectd-sasarray: no attached storage subsystem detected, skipping read callback')])

def test_dispatch_no_scsi_host_exception(csas):
    # Providing no SCSI hosts, expected 0 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_scsi_host.json'
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/0_enclosures'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0])],
            any_order=True)

        csas.collectd.error.assert_has_calls(
            [call(u'PCI controller 02:00.0 has no SCSI host mapped to it'),
            call(u'PCI controller 02:00.0 has no SCSI host mapped to it'),
            call(u'PCI controller 02:00.0 has no SCSI host mapped to it')])


def test_dispatch_no_sas_port(csas):
    # Providing no SAS ports, expected 1 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_sas_port.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[1])],
            any_order=True)

        csas.collectd.error.assert_has_calls(
            [call(u'No SAS port on PCI controller 02:00.0'),
             call(u'No SAS port on PCI controller 02:00.0'),
             call(u'No SAS port on PCI controller 02:00.0')])


def test_dispatch_no_sas_port_exception(csas):
    # Providing no SAS ports, expected 0 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_sas_port.json'
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/0_enclosures'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0])],
            any_order=True)


        csas.collectd.error.assert_has_calls(
            [call(u'No SAS port on PCI controller 02:00.0'),
             call(u'No SAS port on PCI controller 02:00.0'),
             call(u'No SAS port on PCI controller 02:00.0')])


def test_dispatch_no_ports_mapped_on_scsi_host(csas):
    # Providing no SAS ports, expected 1 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_ports_mapped_on_scsi_host.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[1])],
            any_order=True)

        csas.collectd.error.assert_has_calls(
            [call(u'SCSI host host0 on PCI controller 02:00.0 has no SAS port mapped to it'),
             call(u'SCSI host host0 on PCI controller 02:00.0 has no SAS port mapped to it'),
             call(u'SCSI host host0 on PCI controller 02:00.0 has no SAS port mapped to it')])


def test_dispatch_no_ports_mapped_on_scsi_host_exception(csas):
    # Providing no SAS ports, expected 0 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_ports_mapped_on_scsi_host.json'
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/0_enclosures'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0])],
            any_order=True)


        csas.collectd.error.assert_has_calls(
            [call(u'SCSI host host0 on PCI controller 02:00.0 has no SAS port mapped to it'),
             call(u'SCSI host host0 on PCI controller 02:00.0 has no SAS port mapped to it'),
             call(u'SCSI host host0 on PCI controller 02:00.0 has no SAS port mapped to it')])

def test_dispatch_no_sas_enclosure_attached_to_port(csas):
    # Providing no SAS ports, expected 1 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_sas_enclosure_attached_to_port.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()


        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[1])],
            any_order=True)

        csas.collectd.error.assert_has_calls([
            call(u'No SAS enclosure attached to port 2 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 0 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 2 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 0 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 2 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 0 on PCI controller 02:00.0 mapped to SCSI host0')],
            any_order=True)


def test_dispatch_no_sas_enclosure_attached_to_port_exception(csas):
    # Providing no SAS ports, expected 0 enclosure
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/no_sas_enclosure_attached_to_port.json'
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/0_enclosures'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0])],
            any_order=True)

        csas.collectd.error.assert_has_calls([
            call(u'No SAS enclosure attached to port 2 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 0 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 2 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 0 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 2 on PCI controller 02:00.0 mapped to SCSI host0'),
            call(u'No SAS enclosure attached to port 0 on PCI controller 02:00.0 mapped to SCSI host0')],
            any_order=True)


def test_dispatch_psu_wrong(csas):
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/3_psu_wrong.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()


        calls = [
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[2]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0072000546', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0071000183', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0071000183', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4490]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_1_psu', values=[1]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_5_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_0_psu', values=[1]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_1_psu', values=[1]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4610]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_5_fan', values=[0])]

        val_mock.assert_has_calls(calls, any_order=True)

def test_dispatch_esc_message(csas):
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/1_psu_wrong_custom_esc.json'
    csas.open.ESC_FAILURE_MESSAGES_CONTENT_FILE = 'esc_failure_messages_json/test.json'
    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        calls = [
            call(plugin='sasarray'),
             call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[2]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0071000183', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0071000183', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0072000546', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_5_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4610]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_1_psu', values=[1]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_5_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4490]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_2_fan', values=[0])]
        val_mock.assert_has_calls(calls, any_order=True)



def test_dispatch_fan_wrong(csas):
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/1_fan_wrong.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        calls = [
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[2]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0072000546', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0071000183', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0071000183', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4490]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_4_fan', values=[1]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_5_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4610]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_5_fan', values=[0])]
        val_mock.assert_has_calls(calls, any_order=True)

def test_dispatch_fan_wrong_esc(csas):
    # Esc fan on Enclosure 2 shouldn't fail, despite weird status
    csas.SsCli.S2CLI_READ_CONTENT_FILE = 's2cli/1_fan_wrong_custom_esc.json'
    csas.open.ESC_FAILURE_MESSAGES_CONTENT_FILE = 'esc_failure_messages_json/test.json'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        calls = [
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[2]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0072000546', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0071000183', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0071000183', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_5_fan', values=[1]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_5_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4490]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4610]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510])]
        val_mock.assert_has_calls(calls, any_order=True)


def test_dispatch(csas):
    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()



        calls = [
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[2]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0072000546', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0072000546', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='drives', type='count', type_instance=u'RJ0071000183', values=[24]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', type_instance=u'RJ0071000183', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4490]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0072000546', values=[4510]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0072000546_esc_5_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_0_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_1_psu', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_2_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_2_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_3_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4480]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_3_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_4_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4610]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_4_fan', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance=u'esc_5_fan', type='fanspeed', type_instance=u'RJ0071000183', values=[4460]),
            call().dispatch(plugin='sasarray', plugin_instance='esc_error', type='count', type_instance=u'RJ0071000183_esc_5_fan', values=[0])]
        val_mock.assert_has_calls(calls, any_order=True)


def test_dispatch_no_sas_port_on_pci_exclusion_no_log_no_crash(csas):
    # Providing no output after s2cli filtered controllers should not lead to an exception
    csas.SsCli.S2CLI_READ_CONTENT_FILE = None
    csas.open.EXCEPTIONS_LIST_CONTENT_FILE = 'exceptions_list_json/0_enclosures'

    with patch('collectd.Values') as val_mock:
        csas.configure_callback(Mock(children=[]))
        csas.read_callback()

        val_mock.assert_has_calls([
            call(plugin='sasarray'),
            call().dispatch(plugin='sasarray', plugin_instance='enclosures', type='count', values=[0]),
            call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0])],
            any_order=True)


# def test_dispatch_drives(csas):
#     with patch('collectd.Values') as val_mock:
#         csas.configure_callback(Mock(children=[]))
#         csas.read_callback()

#         print('collectd metrics:', val_mock.mock_calls)
#         print('collectd errors', csas.collectd.errors.mock_calls)
#         print('collectd info', csas.collectd.info.mock_calls)
        
#         calls = [
#             call(plugin='sasarray'),
#             call().dispatch(plugin='sasarray', plugin_instance='missing_enclosures', type='count', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance='missing_drives', type='count', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0071000183', type='count', type_instance=u'esc_4_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0071000183', type='count', type_instance=u'esc_5_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0071000183', type='count', type_instance=u'esc_0_psu_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0071000183', type='count', type_instance=u'esc_1_psu_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0071000183', type='count', type_instance=u'esc_2_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0071000183', type='count', type_instance=u'esc_3_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0072000546', type='count', type_instance=u'esc_4_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0072000546', type='count', type_instance=u'esc_5_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0072000546', type='count', type_instance=u'esc_0_psu_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0072000546', type='count', type_instance=u'esc_1_psu_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0072000546', type='count', type_instance=u'esc_2_fan_error', values=[0]),
#             call().dispatch(plugin='sasarray', plugin_instance=u'RJ0072000546', type='count', type_instance=u'esc_3_fan_error', values=[0])]
#         val_mock.assert_has_calls(calls, any_order=True)
