# megaraidsas Collectd plugin
## Configuration
*   ``INTERVAL``: Interval in seconds to get the metrics data. (optional, default: `300`)
*   ``LSMOD_PATH``: Path to the lsmod binary (optional, default: `/usr/sbin/lsmod`)
*   ``LSSCSI_PATH``: Path to the lsscsi binary (optional, default: `/usr/bin/lsscsi`)
*   ``SAS2IRCU_PATH``: Path to the sas2ircu binary (optional, default: `/usr/sbin/sas2ircu`)
*   ``SAS2IRCU_PATH``: Path to the facter binary (optional, default: `/opt/puppetlabs/bin/facter`)

*   ``EXCEPTIONS_LIST_PATH``: Path to the exception list (optional, default: `/etc/collectd-sasarray/exceptions_list.json`)
*   ``ESC_FAILURE_MESSAGES_PATH``: Path to the ESC failure messages list (optional, default: `/etc/collectd-sasarray/esc_failure_messages.json`)
*   ``PCI_IDS_EXCLUSION_LIST_PATH``: Path to the PCI IDs exclusion list (optional, default: `/etc/collectd-sasarray/pci_ids_exclusion_list.json`)
*   ``SUPPORTED_HBA_DRIVERS``: Pipe-delimited suppoorted HBA drivers (optional, default: `mpt2sas|mpt3sas|pm80xx`)

Example:
```python
<Plugin "python">
   Import "collectd_sasarray"
  <Module "collectd_sasarray">
    INTERVAL 300
    LSMOD_PATH "/usr/sbin/lsmod"
    LSSCSI_PATH "/usr/bin/lsscsi"
    SAS2IRCU_PATH  "/usr/sbin/sas2ircu"
    EXCEPTIONS_LIST_PATH  "/etc/collectd-sasarray/exceptions_list.json"
    ESC_FAILURE_MESSAGES_PATH "/etc/collectd-sasarray/esc_failure_messages.json"
    PCI_IDS_EXCLUSION_LIST_PATH "/etc/collectd-sasarray/pci_ids_exclusion_list.json"
    SUPPORTED_HBA_DRIVERS "mpt2sas|mpt3sas|pm80xx"
  </Module>
</Plugin>
```

## System metrics

### Enclosure count
*   **Plugin**: `sasarray`
*   **PluginInstance**: `enclosures`
*   **Type**: `count`
*   **Value**: number of enclosures

####  Example:
```
[root@lxfsrd04c02 ~]# collectdctl listval | grep enclosures
lxfsrd08c04.cern.ch/sasarray-enclosures/count
```

### Enclosure failures
*   **Plugin**: `sasarray`
*   **PluginInstance**: `missing_enclosures`
*   **Type**: `count`
*   **Value**: `0` (ok) | `1` (failure)

####  Example:
```
[root@lxfsrd04c02 ~]# collectdctl listval | grep enclosures
lxfsrd08c04.cern.ch/sasarray-missing_enclosures/count
```

## Enclosures metrics

### Enclosure drives count
*   **Plugin**: `sasarray`
*   **PluginInstance**: `drives`
*   **Type**: `count`
*   **TypeInstance**: enclosure id
*   **Value**: number of drives

####  Example:
```
[root@lxfsrd04c02 ~]# collectdctl listval | grep count-drives
lxfsrd08c04.cern.ch/sasarray-RJ0071000183/count-drives
```

### Enclosure drives failure
*   **Plugin**: `sasarray`
*   **PluginInstance**: `missing_drives`
*   **Type**: `count`
*   **TypeInstance**: enclosure id
*   **Value**: `0` (ok) | `1` (failure)

####  Example:
```
[root@lxfsrd04c02 ~]# collectdctl listval | grep count-drives
lxfsrd08c04.cern.ch/sasarray-RJ0071000183/count-missing_drives
```

### Fans speed
*   **Plugin**: `sasarray`
*   **PluginInstance**: `esc_` + ESC ID + `_fan`
*   **Type**: `fanspeed`
*   **TypeInstance**: enclosure id
*   **Value**: fan speed

####  Example:
```
[root@lxfsrd04c02 ~]# collectdctl listval | grep enclosures
lxfsrd08c04.cern.ch/sasarray-RJ0071000183/fanspeed-esc_01_fan
```

### ESC device failure
*   **Plugin**: `sasarray`
*   **PluginInstance**: `esc_error`
*   **Type**: `count`
*   **TypeInstance**: enclosure id + `_esc_` + esc id + `_` + esc type
*   **Value**: `0` (ok) | `1` (failure)

####  Example:
```
[root@lxfsrd04c02 ~]# collectdctl listval | grep enclosures
lxfsrd08c04.cern.ch/sasarray-RJ0071000183/count-esc_01_fan_error
lxfsrd08c04.cern.ch/sasarray-RJ0071000183/count-esc_01_psu_error
lxfsrd08c04.cern.ch/sasarray-RJ0071000184/count-esc_03_psu_error
lxfsrd08c04.cern.ch/sasarray-RJ0071000184/count-esc_04_fan_error
```