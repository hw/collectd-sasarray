""" Collectd plugin to monitor SAS arrays """
from __future__ import print_function
import subprocess
import re
import json
import yaml

import collectd

from sscli.s2cli import SsCli


# Default config
PLUGIN_NAME = 'sasarray'
INTERVAL = 300
LSMOD_PATH = '/usr/sbin/lsmod'
SAS2IRCU_PATH = '/usr/sbin/sas2ircu'
EXCEPTIONS_LIST_PATH = '/etc/collectd-sasarray/exceptions_list.json'
ESC_FAILURE_MESSAGES_PATH = '/etc/collectd-sasarray/esc_failure_messages.json'
PCI_IDS_EXCLUSION_LIST_PATH = '/etc/collectd-sasarray/pci_ids_exclusion_list.json'
FACTER_PATH = '/opt/puppetlabs/bin/facter' 
SUPPORTED_HBA_DRIVERS = 'mpt2sas|mpt3sas|pm80xx'
PUPPET_FACTS_CACHE_PATH = '/opt/puppetlabs/puppet/cache/hardware_facts_cache.yaml'
PUPPET_FACTS_KEY = 'productserialassettag'

# Global caches
LSMOD_OUTPUT = None
SERVER_ID = None
SERVER_ASSET_TAG = None
SERVER_SERIAL = None
ESC_FAILURE_MESSAGES = None
EXCLUDED_PCI_CONTROLLERS = None

# Expected default values for enclosures: >1
EXPECTED_NUMBER_OF_ENCLOSURES = -1
EXPECTED_NUMBER_OF_DRIVERS_PER_ENCLOSURE = -1
LOADED_HBA_DRIVERS = []

# REGEXES
SERVER_ID_RE = re.compile(r"^(?P<asset_tag>[A-z]+[0-9]*)-(?P<serial>.*)$")


def get_server_id_from_puppet_cache():
    """ Retrieve product serial and asset tag identifier with puppet cache. """
    try:
        with open(PUPPET_FACTS_CACHE_PATH, 'r') as stream:
            return yaml.safe_load(stream)[PUPPET_FACTS_KEY]
    except yaml.YAMLError as e:
        collectd.error("can't parse YAML file %s: %s" % (PUPPET_FACTS_CACHE_PATH, e))
    except TypeError as e:
        collectd.error("empty file or can't find key %s in %s" % (PUPPET_FACTS_KEY, PUPPET_FACTS_CACHE_PATH))    
    except IOError as e:
        collectd.error("can't find file %s" % PUPPET_FACTS_CACHE_PATH)

    return False

def get_server_id_from_facter():
    """ Retrieve product serial and asset tag identifier using facter. """

    try:
        res = subprocess.Popen([FACTER_PATH, '-p', PUPPET_FACTS_KEY], stdout=subprocess.PIPE)
        value = res.stdout.readline().strip()
    except Exception as e:
        collectd.error('exception while retrieving facter output: %s' % str(e))
        return False

    if re.search('UNDEF', value):
        collectd.error("invalid output of '%s -p %s': %s" % (FACTER_PATH, PUPPET_FACTS_KEY, value))
        return False

    return value


def get_lsmod():
    """ Retrieve output of lsmod. """

    try:
        res = subprocess.Popen(LSMOD_PATH, stdout=subprocess.PIPE)
        value = res.stdout.read()

        # handle python3 bytes output
        if isinstance(value, bytes):
            value = value.decode()
    except Exception as e:
        collectd.error('exception while retrieving lsmod output: %s' % str(e))
        return False

    if not value:
        collectd.error('lsmod returned an invalid output')
        return False

    return value


def get_config_match(config):
    """ Filter config lists. """

    # Full search for specific SERVER_ID match (has priority)
    for identifier, config_data in config.items():
        if identifier == SERVER_ID:
            return config_data
    
    # Full search for SERVER_ASSET_TAG (Contract ID)
    for identifier, config_data in config.items():
        if identifier == SERVER_ASSET_TAG:
            return config_data

    return []


def get_lsi_hba_raid_support():
    """ Checks if loaded mpt2sas HBA is supporting RAID functionalities. """
    try:
        res = subprocess.Popen([SAS2IRCU_PATH, 'list'], 
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        stderr, stdout = res.stderr.read(), res.stdout.read()
    except Exception as e:
            collectd.error("exception while retrieveing '%s list': %s" % 
                           (SAS2IRCU_PATH, str(e)))
            return None

    # Check that stderr is empty
    if stderr:
        collectd.error("'sas2ircu list' stderr is not empty: %s" % stderr)
        return None
    
    # Check that stdout is not empty
    if not stdout:
        collectd.error("sas2ircu list' stdout is empty")
        return None
    
    # Compile list of HBA IDs
    lsi_hbas = []
    matches = re.finditer(r"^\s+([\d]+).+$", stdout, re.MULTILINE)
    for match in matches:
        lsi_hbas.append(match.group(1)[0])

    # Check RAID support of every HBA
    for lsi_hba in lsi_hbas:
        try:
            res = subprocess.Popen([SAS2IRCU_PATH, lsi_hba, 'display'], 
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            stderr, stdout = res.stderr.read(), res.stdout.read()
        except Exception as e:
            collectd.error("exception while retrieveing '%s %s display': %s" % 
                        (SAS2IRCU_PATH, lsi_hba, str(e)))
            return None
        
        # Check that stderr is empty
        if stderr:
            collectd.error("'sas2ircu %s display' stderr is not empty: %s" % (lsi_hba, stderr))
            return None
        
        # Check that stdout is not empty
        if not stdout:
            collectd.error("sas2ircu %s display' stdout is empty" % lsi_hba)
            return None
        
        match = re.search(r"\s+RAID\s+Support\s+:\s+Yes$", stdout, re.MULTILINE)
        if match:
            return True

    return False


def get_enclosures(storage_data):
    enclosures = {}

    # If no data, return no enclosures
    if not storage_data:
        return enclosures

    # Traverse attached storage data and count enclosures
    for pci_controller_id, pci_controller in storage_data.items():
        if not pci_controller:
            collectd.error("PCI controller %s has no SCSI host mapped to it" % 
                           pci_controller_id)
            continue

        for scsi_host_id, scsi_host in pci_controller.items():
            if not scsi_host:
                collectd.error("SCSI host %s on PCI controller %s has no SAS "\
                               "port mapped to it" % (scsi_host_id, 
                                                      pci_controller_id))
                continue

            if not scsi_host['controller_ports']:
                collectd.error("No SAS port on PCI controller %s" % 
                               pci_controller_id)
                continue

            for port_id, sas_enclosures in scsi_host['controller_ports'].items():
                if not sas_enclosures:
                    collectd.error("No SAS enclosure attached to port %s on "
                                   "PCI controller %s mapped to SCSI %s" % 
                                   (port_id, pci_controller_id, scsi_host_id))
                    continue

                enclosures.update(sas_enclosures.items())

    return enclosures


def dispatch_sas_enclosures(storage_data, metrics):
    number_of_enclosures = len(get_enclosures(storage_data))

    # Dispatch number of enclosures
    metrics.dispatch(plugin=PLUGIN_NAME,
                     plugin_instance='enclosures',
                     type='count',
                     values=[number_of_enclosures])

    if EXPECTED_NUMBER_OF_ENCLOSURES == -1:
        # If no exception is defined, node should have at least 1 SAS enclosure
        if number_of_enclosures < 1:
            metrics.dispatch(plugin=PLUGIN_NAME,
                             plugin_instance='missing_enclosures',
                             type='count',
                             values=[1])
            return
    else:
        # In case an exception is defined, match against it
        if number_of_enclosures != EXPECTED_NUMBER_OF_ENCLOSURES:
            metrics.dispatch(plugin=PLUGIN_NAME,
                             plugin_instance='missing_enclosures',
                             type='count',
                             values=[1])
            return
    
    # If number of enclosure is as expected
    metrics.dispatch(plugin=PLUGIN_NAME,
                     plugin_instance='missing_enclosures',
                     type='count',
                     values=[0])


def dispatch_esc_devices(storage_data, metrics):
    """ Dispatch metrics related to PSUs and fans. """
    # Traverse attached storage data and check ESC devices
    enclosures = get_enclosures(storage_data)

    for enclosure_id, enclosure in enclosures.items():
        if not enclosure:
            collectd.error("SAS enclosure %s can't be fully identified" % enclosure_id)
            continue
        
        if not enclosure['esc_devices']:
            collectd.error("SAS enclosure %s has no ESC devices" % enclosure_id)
            continue
        
        try:
            esc_failure_message = ESC_FAILURE_MESSAGES[enclosure['product_id']]
        except KeyError:
            esc_failure_message = False

        for esc_device_id, esc_device in enclosure['esc_devices'].items():
            metric_value = 0

            if esc_device['type'] == 'Power supply':
                esc_device['type'] = 'psu'

                # Raise PSU alarm if status is not OK
                if esc_device['status'] != 'OK':
                    # Raise PSU alarm if esc_failure_message is not set or
                    # corresponds to status
                    if not esc_failure_message or \
                        esc_device['status'] == esc_failure_message:
                        metric_value = 1

            elif esc_device['type'] == 'Cooling':
                esc_device['type'] = 'fan'

                try:     
                    fan_speed = int(esc_device['value'].replace(' rpm', ''))
                    metrics.dispatch(plugin=PLUGIN_NAME,
                                     plugin_instance='esc_%s_fan' % esc_device_id,
                                     type='fanspeed',
                                     type_instance=enclosure_id,
                                     values=[fan_speed])
                except:
                    pass

                # Raise fan alarm if status is not OK or Not Installed
                if esc_device['status'] != 'OK' and \
                    esc_device['status'] != 'Not installed':
                    # Raise fan alarm if esc_failure_message is not set or
                    # corresponds to status
                    if not esc_failure_message or \
                        esc_device['status'] == esc_failure_message:
                        metric_value = 1

            else:
                collectd.info('Unable to identify ESC device %s: %s' % 
                              (esc_device_id, esc_device))

            type_instance = '%s_esc_%s_%s' % (enclosure_id,
                                              esc_device_id, 
                                              '_'.join(esc_device['type']
                                                 .lower().split()))
            metrics.dispatch(plugin=PLUGIN_NAME,
                             plugin_instance='esc_error',
                             type='count',
                             type_instance=type_instance,
                             values=[metric_value])


def dispatch_drives(storage_data, metrics):
    """ Dispatch metrics related to drives. """

    # Traverse attached storage data and check drives
    enclosures = get_enclosures(storage_data)
    available_physical_ports = 0

    for enclosure_id, enclosure in enclosures.items():
        detected_drives = 0

        if not enclosure:
            collectd.error("SAS enclosure %s can't be fully identified" % enclosure_id)
            continue
        
        if not enclosure['physical_sas_ports']:
            collectd.error("SAS enclosure %s has no ESC devices" % enclosure_id)
            
        try:
            available_physical_ports = int(enclosure['available_physical_ports'])
        except KeyError:
            collectd.error("can't find number of physical ports for SAS enclosure %s" % enclosure_id)
        except ValueError:
            collectd.error("can't convert number of physical ports for SAS enclosure %s" % enclosure_id)

        detected_drives += len(enclosure['physical_sas_ports'])

        # Dispatch number of drivers
        metrics.dispatch(plugin=PLUGIN_NAME,
                         plugin_instance='drives',
                         type='count',
                         type_instance=enclosure_id,
                         values=[detected_drives])

        if EXPECTED_NUMBER_OF_DRIVERS_PER_ENCLOSURE == -1:
            # If no exception is defined, all the ports should be populated
            if detected_drives < available_physical_ports:
                metrics.dispatch(plugin=PLUGIN_NAME,
                                 plugin_instance='missing_drives',
                                 type='count',
                                 type_instance=enclosure_id,
                                 values=[1])
                continue
        else:
            # In case an exception is defined, match against it
            if detected_drives != EXPECTED_NUMBER_OF_DRIVERS_PER_ENCLOSURE:
                metrics.dispatch(plugin=PLUGIN_NAME,
                                 plugin_instance='missing_drives',
                                 type='count',
                                 type_instance=enclosure_id,
                                 values=[1])
                continue
        
        # If number of enclosure is as expected
        metrics.dispatch(plugin=PLUGIN_NAME,
                         plugin_instance='missing_drives',
                         type='count',
                         type_instance=enclosure_id,
                         values=[0])


def configure_callback(config):
    """ Configure the sensor using the callback from collectd. """
    global INTERVAL
    global LSMOD_PATH, LSMOD_OUTPUT
    global SAS2IRCU_PATH
    global SUPPORTED_HBA_DRIVERS
    global EXCEPTIONS_LIST_PATH, ESC_FAILURE_MESSAGES_PATH
    global PCI_IDS_EXCLUSION_LIST_PATH
    global PUPPET_FACTS_CACHE_PATH, PUPPET_FACTS_KEY
    global FACTER_PATH

    # Global stores
    global ESC_FAILURE_MESSAGES
    global EXPECTED_NUMBER_OF_ENCLOSURES, EXCLUDED_PCI_CONTROLLERS
    global EXPECTED_NUMBER_OF_DRIVERS_PER_ENCLOSURE
    global SERVER_ID, SERVER_ASSET_TAG, SERVER_SERIAL
    global LOADED_HBA_DRIVERS

    exceptions_list = None
    pci_ids_exclusion_list = None

    # Configuring sensor
    for node in config.children:
        if node.key == 'INTERVAL':
            INTERVAL = int(node.values[0])
        elif node.key == 'LSMOD_PATH':
            LSMOD_PATH = node.values[0]
        elif node.key == 'EXCEPTIONS_LIST_PATH':
            EXCEPTIONS_LIST_PATH = node.values[0]
        elif node.key == 'ESC_FAILURE_MESSAGES_PATH':
            ESC_FAILURE_MESSAGES_PATH = node.values[0]
        elif node.key == 'PCI_IDS_EXCLUSION_LIST_PATH':
            PCI_IDS_EXCLUSION_LIST_PATH = node.values[0]
        elif node.key == 'SAS2IRCU_PATH':
            SAS2IRCU_PATH = node.values[0]
        elif node.key == 'PUPPET_FACTS_CACHE_PATH':
            PUPPET_FACTS_CACHE_PATH = node.values[0]
        elif node.key == 'PUPPET_FACTS_KEY':
            PUPPET_FACTS_KEY = node.values[0]
        elif node.key == 'FACTER_PATH':
            FACTER_PATH = node.values[0]
        elif node.key == 'SUPPORTED_HBA_DRIVERS':
            SUPPORTED_HBA_DRIVERS = node.values[0]

    # Retrieve product serial and asset tag
    SERVER_ID = get_server_id_from_puppet_cache()
    if not SERVER_ID:
        SERVER_ID = get_server_id_from_facter()
    if SERVER_ID:
        # Separate asset tag and serial number
        matches = SERVER_ID_RE.match(SERVER_ID)
        if matches:
            SERVER_ASSET_TAG = matches.group('asset_tag')
            SERVER_SERIAL = matches.group('serial')

    # Read exceptions list
    try:
        with open(EXCEPTIONS_LIST_PATH) as stream:
            exceptions_list = json.loads(stream.read())
            server_exceptions = get_config_match(exceptions_list)
            if server_exceptions:
                EXPECTED_NUMBER_OF_ENCLOSURES = int(server_exceptions['total_amount_of_sas_enclosures'])
                EXPECTED_NUMBER_OF_DRIVERS_PER_ENCLOSURE = int(server_exceptions['drives_per_sas_enclosure'])
    except Exception as e:
        collectd.info("exception while retrieving exception config: %s" % str(e))

    # Read ESC failure messages and PCI IDs exclusion list
    try:
        with open(ESC_FAILURE_MESSAGES_PATH) as stream:
            ESC_FAILURE_MESSAGES = json.loads(stream.read())
        with open(PCI_IDS_EXCLUSION_LIST_PATH) as stream:
            pci_ids_exclusion_list = json.loads(stream.read())
        EXCLUDED_PCI_CONTROLLERS = get_config_match(pci_ids_exclusion_list)
    except ValueError:
        collectd.error("exception while parsing a config files: JSON is not valid")
    except Exception as e:
        collectd.error("exception while retrieving config files: %s" % str(e))
    
    # Retrieve list of loaded modules and retrieve loaded HBA drivers
    LSMOD_OUTPUT = get_lsmod()
    if LSMOD_OUTPUT:
        LOADED_HBA_DRIVERS = re.findall(r"^(%s)" % SUPPORTED_HBA_DRIVERS, 
                                        LSMOD_OUTPUT, 
                                        re.MULTILINE)

    # Discarding DELL PERC mpt2sas HBAs (fake RAID)
    if 'mpt2sas' in LOADED_HBA_DRIVERS:
        lsi_hba_support = get_lsi_hba_raid_support()
        if lsi_hba_support:
            collectd.info("excluding LSI HBA because RAID support is Enabled"
                          " (DELL PERC node?)")
            LOADED_HBA_DRIVERS.remove('mpt2sas')
        if lsi_hba_support is None:
            collectd.error("can't detect LSI HBA RAID support (mpt2sas only)")

    # Test SCSI attached storage subsystem
    sscli_check = True
    any_attached_storage = False
    try:
        if SsCli(EXCLUDED_PCI_CONTROLLERS).get_attached_storage_subsystem():
            any_attached_storage = True
    except Exception as e:
        collectd.error('error in retrieving attached storage subsystem: %s' % str(e))
        sscli_check = False


    # Register collected read function
    if not SERVER_ID:
        collectd.error("can't get key '%s' from facter or Puppet cache" % PUPPET_FACTS_KEY)
    if not SERVER_ASSET_TAG or not SERVER_SERIAL:
        collectd.error("can't parse serial and asset tag from %s" % SERVER_ID)
    elif not LSMOD_OUTPUT:
        collectd.error("can't get loaded modules with %s" % LSMOD_PATH)
    elif not ESC_FAILURE_MESSAGES:
        collectd.error("can't get ESC failure messages list from %s" % ESC_FAILURE_MESSAGES_PATH)
    elif not exceptions_list:
        collectd.error("can't get exception list from %s" % EXCEPTIONS_LIST_PATH)
    elif not pci_ids_exclusion_list:
        collectd.error("can't get PCI IDs exclusion list from %s" % PCI_IDS_EXCLUSION_LIST_PATH)
    elif not LOADED_HBA_DRIVERS:
        collectd.error("no supported HBA drivers loaded, skipping read callback")
    elif not sscli_check:
        collectd.error("unable to retrieve attached storage subsystem with SSCli")
    elif not any_attached_storage:
        collectd.info("collectd-sasarray: no attached storage subsystem detected, skipping read callback")
    else:
        collectd.register_read(read_callback, INTERVAL)


def read_callback():
    """ Read the file, search for failures and send the metrics. """

    # Instantiate collectd Values for metric dispatch
    metrics = collectd.Values(plugin=PLUGIN_NAME)

    # TODO: Server exception list to be managed

    # Loading SCSI attached storage subsystem
    sscli = SsCli(EXCLUDED_PCI_CONTROLLERS)
    attached_storage_subsystem = sscli.get_attached_storage_subsystem()

    # Checking the expected amount of SAS enclosures
    dispatch_sas_enclosures(attached_storage_subsystem, metrics)

    # Checking drives count on physical ports
    dispatch_drives(attached_storage_subsystem, metrics)

    # Checking ESC devices (fans, PSUs)
    dispatch_esc_devices(attached_storage_subsystem, metrics)


collectd.register_config(configure_callback)