Name:      collectd-sasarray
Version:   22.1.3
Release:   1%{?dist}
Summary:   Collectd plugin for SAS JBOD arrays monitoring
Vendor:    CERN
License:   MIT
Group:     System Environment/Base
URL:       https://gitlab.cern.ch/hw/collectd-sasarray
Source:    %{name}-%{version}.tgz

BuildArch:      noarch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root

AutoReq:        no


%if 0%{?el7}
%global py_version python2
%define __python /usr/bin/python2
BuildRequires:  python2-devel
BuildRequires:  python3-devel
BuildRequires:  python-setuptools
Requires:       python >= 2.7
%else
%if 0%{?el8} || 0%{?el9}
%global py_version python3
%define __python /usr/bin/python3
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
Requires:       python(abi) >= 3
%endif
%endif

Requires:       collectd
Requires:       selinux-policy-targeted-collectd-hw-plugins >= 18.11.4
Requires:       s2cli

%description
Collectd Plugin to monitor SAS JBOD arrays


%prep
%setup -q

%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{python_sitelib}/*
%doc AUTHORS COPYING

%changelog
* Thu Jan 13 2022 - Luca Gardi <luca.gardi@cern.ch> - 22.1.3-1
- deploy on CentOS 9 Stream

* Wed Apr 14 2021 - Luca Gardi <luca.gardi@cern.ch> - 21.4.3-1
- deploy on c8s
- drop requirement on python-devel-tools
- drop slc6 support

* Thu Oct 29 2020 - Luca Gardi <luca.gardi@cern.ch> - 20.10.5-2
- fix issue with py3 bytes output of lsmod

* Tue Oct 27 2020 - Luca Gardi <luca.gardi@cern.ch> - 20.10.5-1
- rpm: fix wrong python3 requirement

* Wed Sep 2 2020 - Luca Gardi <luca.gardi@cern.ch> - 20.9.1-1
- deploy with rpmci
- c8 compatibility

* Thu Jan 16 2020 - Luca Gardi <luca.gardi@cern.ch> - 20.1.3-1
- do not register read callback if no storage subsystem present
- add test case for no SCSi attached storage after exclusion

* Thu Dec 05 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.12.1-1
- fix exc on None from SsCli (all PCI IDs excluded)

* Thu May 23 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.5.4-1
- get_server_id_from_puppet_cache: added function
- configure_callback: add PUPPET_FACTS_CACHE and PUPPET_FACTS_KEY args

* Mon May 13 2019 - Herve Rousseau <hroussea@cern.ch> - 19.4.2-1
- add facter -p to query puppet fact (productserialassettag)

* Thu Apr 04 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.4.1-2
- add dependency on s2cli
- modified path for facter

* Fri Mar 29 2019 - Luca Gardi <luca.gardi@cern.ch> - 19.3.5-1
- Initial release
